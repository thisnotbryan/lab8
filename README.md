GIT REPO = https://bitbucket.org/thisnotbryan/lab8/src/master/

1.You can specify different destinations to write to. Different appenders write to a file, roll the file for given time periods, write to a queue or database, etc.

You can specify a consistent format for log messages instead of having to add it to every line you write to stdout.

You can choose an appender that buffers the output so that multiple threads can log without having the threads contend for the lock on the console object.

You can filter different levels of Logs

2.It comes from a Condition evaluator class part of JUNIT

3.Asserts that execution of the supplied executable throws an exception of the expectedType and returns the exception.

If no exception is thrown, or if an exception of a different type is thrown, this method will fail.

4.1. To ensure that during deserialization the same class (that was used during serialize process) is loaded.

4.2. The constructor must either catch the exception thrown by the superclass's constructor or it must declare that it throws that same exception and you do not "inherit" non-default constructors you need to define the one taking a String in your class. 

4.3. If SuperClass does not declare an exception, then the SubClass can only declare unchecked exceptions, but not the checked exceptions

5.It configures a logger named "edu.baylor.ecs.si.Timer" and instaniates its properties

6.Markdown is a text-to-HTML conversion tool for web writers. Markdown allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML). It is used in bitbucket to display the README file on the homepage of the repository 

7.The compiler could not see that it was throwing the TimerException. I removed the finally block.

8.The finally block is the issue, the code first throws the correct exception but then the finally block is executed which creates then throws a new exception which is not the expected one.

11.Timer Exception is a user-defined exception, NullPointerException is a build-in exception